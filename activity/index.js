console.log("Hello World!");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

    function personalDetails(){
        let fullName = prompt("What is your name?");
        let currentAge = prompt("How old are you?");
        let location = prompt("Where do you live?");

        console.log("Hello , " + fullName);
        console.log("You are " + currentAge + " years old.");
        console.log("You live in " + location);
    }
    personalDetails();
    alert("Thank you for your answers.")
    
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

    function favoriteArtists(){
        let Artist1 = "1. Taylor Swift";
        let Artist2 = "2. Harry Styles";
        let Artist3 = "3. Ariana Grande";
        let Artist4 = "4. Ed Sheeran";
        let Artist5 = "5. BTS";

        console.log(Artist1); 
        console.log(Artist2);
        console.log(Artist3);
        console.log(Artist4);
        console.log(Artist5);

    }
    favoriteArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

    function favoriteMovies(){
        let favoriteMovie1 = "1. Hustle"
        console.log(favoriteMovie1);
        console.log("Rotten Tomatoes Rating: 92%");

        let favoriteMovie2 = "2. Doctor Strange"
        console.log(favoriteMovie2);
        console.log("Rotten Tomatoes Rating: 89%");

        let favoriteMovie3 = "3. Jurassic Park"
        console.log(favoriteMovie3);
        console.log("Rotten Tomatoes Rating: 92%");

        let favoriteMovie4 = "4. The Power of the Dog"
        console.log(favoriteMovie4);
        console.log("Rotten Tomatoes Rating: 94%");

        let favoriteMovie5 = "5. Star Wars: The Force Awakens"
        console.log(favoriteMovie5);
        console.log("Rotten Tomatoes Rating: 93%");
    }
    favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
    alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");
    
	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);
