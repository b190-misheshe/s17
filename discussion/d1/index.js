// console.log("Hello World");

// // FUNCTION DECLARATION
// function printName(){
//     console.log("My name is John");
// };

// printName();

// declaredFunction();

// function declaredFunction(){
//     console.log("Hello from declaredFunction");
// };

// let variableFunction = function(){
//     console.log("Hello again!");
// };

// variableFunction();

// function animeFavorite(){
//     console.log("Recommended anime: Demon Slayer, Sailor Moon, Naruto");
// };
// animeFavorite();

// function moviesFavorite(){
//     console.log("Movie list: Jurassic Park, Fractured, Escape Room");
// };
// moviesFavorite();

// let animeFavorite1 = function(){
//     console.log("My recommended anime are:");
//     console.log("Naruto");
//     console.log("Jujitsu");
//     console.log("Slayer");
// }

// animeFavorite1();

// // 

// declaredFunction = function(){
//     console.log("updated declaredFunction");
// };
// declaredFunction();

// const constFunction = function(){
//     console.log("Initialized const function");
// };
// constFunction();

// constFunction = function(){
//     console.log("cannot be re-assgined");
// };
// constFunction();


// function showNames(){
//     var functionVar = "Joe";
//     const functionConst = "John";
//     let functionLet = "Jane";

//     console.log(functionVar);
//     console.log(functionConst);
//     console.log(functionLet);  
    
//     console.log(functionVar);
//     console.log(functionConst);
//     console.log(functionLet); 
// };

// showNames();
// console.log(functionVar);
// console.log(functionConst);
// console.log(functionLet); 


// // NESTED FUNCTIONS
// function newFunction(){
//     let name = "Jane";
//     console.log(name);

//     function nestedFunction(){
//         let nestedName = "John";
//         // console.log(name);
//         console.log(nestedName);
//     };
//     // console.log(nestedName;
//     nestedFunction();
// };

// // nestedFunction();
// newFunction();



// function oneFunction(){
//     let activityName = "Activity One";

//     function twoFunction(){
//         let twoFunction = "Activity Two";
//         console.log(oneFunction);
//         console.log(twoFunction);
//     };
// };

// oneFunction();


// alert("Hello World");

// function showSampleAlert(){
//     alert("Hello Again");
// };

// showSampleAlert();
// console.log("I will be displayed after the alert has been closed.");

// alert("Hello World");

// function showSampleAlert(){
//     alert("Hello Again");
// };

// console.log("I will be displayed after the alert has been closed.");
// showSampleAlert();

// let samplePrompt = prompt("Enter your name.");

// console.log("Hello " + samplePrompt);

// function samplePrompt(){
//     prompt("Thank you!");
// };
// samplePrompt();


function welcomeMessage(){
    let firstName = prompt("Enter you first name");
    let lastName = prompt("Enter your last name");

    console.log("Hello " + firstName + " " + lastName);
};

welcomeMessage();

